const { exec, spawn } = require('child_process');
const { defineConfig } = require("cypress");

const docker_exec = (command) => {
  exec(`docker exec fw-mqtt ash -c "${command}"`, (err, output) => {
    if (err) {
      console.log('docker exec error', err);
    }
  });
};

let capturedCommands = [];
let monitor = null;

module.exports = defineConfig({
  e2e: {
    baseUrl: 'http://localhost:8080',
    includeShadowDom: true,
    setupNodeEvents(on, config) {
      // implement node event listeners here
      on('task', {
        controller(options) {
          switch (options.action) {
            case 'activate':
              docker_exec(
                'mosquitto_pub -t /ignite/connected -m true && mosquitto_pub -t /ignite/armed -m false'
              );
              break;
            case 'deactivate':
              docker_exec(
                'mosquitto_pub -t /ignite/connected -m false'
              );
              break;
            case 'reportArmed':
              docker_exec('mosquitto_pub -t /ignite/armed -m true');
              break;
            case 'reportDisarmed':
              docker_exec('mosquitto_pub -t /ignite/armed -m false');
              break;
            case 'reportRelayState':
              docker_exec(`mosquitto_pub -t /ignite/relay/${options.relay} -m ${options.relayState}`);
              break;
            case 'reset':
              docker_exec(
                'mosquitto_pub -t /ignite/connected -m false && mosquitto_pub -t /ignite/armed -m false'
              );
              break;
            default:
          }
          return null;
        },

        monitor(options) {
          switch (options.action) {
            case 'start':
              monitor = spawn('docker', ['exec', 'fw-mqtt', 'mosquitto_sub', '-v', '-t', '/ignite/#']);
              monitor.stdout.on('data', data => {
                capturedCommands.push(data.toString('utf-8').trim());
              });
              break;
            case 'getLogs':
              return capturedCommands;
            case 'reset':
              capturedCommands = [];
              break;
            case 'stop':
              if (monitor) {
                monitor.kill();
                // The kill signal isn't passed along to the process running in the container
                // So kill it manually
                exec(`docker exec fw-mqtt killall mosquitto_sub`);
                monitor = null;
                capturedCommands = [];
              }
              break;
            default:
          }
          return null;
        }
      });
    },
    viewportWidth: 800,
    viewportHeight: 1280
  },
});
