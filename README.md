# Igniter User Interface

This repo contains the user interface for the remote controller fireworks
igniter.  The interface consists of a control panel that can:
- Arm/Disarm the system
- Send fire commands to individual igniters
- Report the status of each igniter
- Keep with which which channels have been fired and how many times

The UI communicates with the controller via MQTT.  You should set the MQTT host
name as `SNOWPACK_PUBLIC_MQTT_URL` in the start script.  Your MQTT server needs
to be accessible via websockets.
