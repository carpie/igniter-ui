import { interpret } from 'xstate';

export class MachineController {
  constructor(host, machine, options = {}) {
    this.host = host;
    host.addController(this);
    if (options.context) {
      this.service = interpret(machine.withContext(options.context), options).start();
    } else {
      this.service = interpret(machine, options).start();
    }
    // Debugging
    // this.service.subscribe(state => {
    //   console.log('Event ->', state.event);
    // });
  }

  get context() {
    return this.service.state?.context;
  }

  get state() {
    return this.service.state;
  }

  get send() {
    return this.service.send;
  }

  hostConnected() {
    if (this.service) {
      this.service.onTransition(state => {
        if (state.changed) {
          this.host.requestUpdate();
        }
      });
    }
  }

  hostDisconnected() {
    if (this.service) {
      this.service.stop();
    }
  }
}
