import { css, html, LitElement } from 'lit';

import { MachineController } from './controllers/MachineController.js';
import { launcherMachine } from './launcherMachine.js';

import './components/ArmButton.js';
import './components/FireButton.js';

export class LauncherApp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        height: 100%;
        max-width: 978px;
      }

      .panel {
        position: relative;
        background-color: #191f2b;
        padding: 1rem;
        margin: 0.5rem 0.7rem 1rem;
        border: solid 3px silver;
        display: flex;
        flex-direction: column;
        text-align: left;
      }

      .panel:before {
        position:absolute; right:-1px; bottom:-1px; content:'';
        border-bottom: 20px solid silver;
        border-left: 20px solid transparent;
      }

      .panel:after {
        position:absolute; right:-3px; bottom:-3px; content:'';
        border-bottom: 20px solid #282c34;
        border-left: 20px solid transparent;
      }

      .panel-header {
        position: relative;
        top: -0.4rem;
        left: -0.4rem;
        font-size: 0.8rem;
        color: #999;
      }

      .label {
        display: inline-block;
        font-size: 0.8rem;
        margin-right: 1.0rem;
        margin-left: 0.5rem;
        color: #999;
        min-width: 6.0rem;
      }

      .status {
        display: inline-block;
        font-size: 1.0rem;
        min-width: 2.0rem;
        color: #999;
      }

      @keyframes pulsate {
        0% { opacity: 0.5; }
        50% { opacity: 0.9; }
        100% { opacity: 0.5; }
      }

      .status[active] {
        color: rgba(19, 211, 69, .9);
        text-shadow: 0 0 4px rgba(19, 211, 69);
      }

      .fire-control {
        display: flex;
        flex-direction: column;
      }

      .button-panel {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
      }

      .clear-button {
        align-self: center;
        outline: none;
        font-family: inherit;
        font-size: 1.2rem;
        margin: 0.5rem;
        height: 3rem;
        width: 15rem;
        border: 1px solid #ccc;
        border-radius: 8px;
        color: #999;
        background-color: #0f1726;
      }
      .clear-button:active {
        border-color: rgba(19, 211, 69, .9);
        box-shadow: 0 0 4px rgba(19, 211, 69);
        color: rgba(19, 211, 69, .9);
        text-shadow: 0 0 4px rgba(19, 211, 69);
      }

      @media (min-width: 768px) {
        .clear-button {
          height: 5rem;
        }
    `;
  }

  constructor() {
    super();
    this.fsm = new MachineController(this, launcherMachine, { devTools: true });
    this.addEventListener('requestFire', event => {
      const { send } = this.fsm;
      const index = parseInt(event.composedPath()[0].getAttribute('index'), 10);
      send({ type: 'REQUEST_FIRE_RELAY', index });
    });
  }

  clearFiredCounts() {
    const { send } = this.fsm;
    send('CLEAR_FIRED_COUNTS');
  }

  requestSystemArm() {
    const { send } = this.fsm;
    send('REQUEST_SYSTEM_ARM');
  }

  requestSystemDisarm() {
    const { send } = this.fsm;
    send('REQUEST_SYSTEM_DISARM');
  }

  render() {
    const { context, state } = this.fsm;
    const { firedCounts, relayStates } = context;
    const networkConnected = state.matches('mqtt.network.connected');
    const controllerActive = state.matches('mqtt.controller.active');
    const systemArmed = controllerActive && state.matches('mqtt.fireControl.armed');

    return html`
      <div class="panel">
        <div class="panel-header">SYSTEM STATUS</div>
        <div class="status-row">
          <span class="label">NETWORK</span>
          <span data-testid="networkStatus" class="status" ?active=${networkConnected}>
            ${networkConnected ? 'CONNECTED' : 'DISCONNECTED'}
          </span>
        </div>
        <div class="status-row">
          <span class="label">CONTROLLER</span>
          <span data-testid="controllerStatus" class="status" ?active=${controllerActive}>
            ${controllerActive ? 'ACTIVE' : 'INACTIVE'}
          </span>
        </div>
        <div class="status-row">
          <span class="label">FIRE CONTROL</span>
          <span data-testid="armedStatus" class="status" ?active=${systemArmed}>
            ${systemArmed ? 'ARMED' : 'DISARMED'}
          </span>
        </div>
      </div>

      <div class="panel">
        <div class="panel-header">SYSTEM CONTROL</div>
        <launcher-arm-button
          ?armed=${systemArmed}
          @requestSystemArm=${this.requestSystemArm}
          @requestSystemDisarm=${this.requestSystemDisarm}
        ></launcher-arm-button>
      </div>

      <div class="panel fire-control">
        <div class="button-panel">
          ${relayStates.map((relayState, index) => {
            return html`
              <launcher-fire-button
                index=${index}
                firing=${relayState}
                firedcount=${firedCounts[index]}
              ></launcher-fire-button>
            `;
          })}
        </div>
        <button class="clear-button" @click=${this.clearFiredCounts}>CLEAR FIRED COUNTS</button>
      </div>
    `;
  }
}

customElements.define('launcher-app', LauncherApp);
