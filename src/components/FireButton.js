import { css, html, LitElement } from 'lit';

import { MachineController } from '../controllers/MachineController.js';

import { fireButtonMachine } from './fireButtonMachine.js';

class FireButton extends LitElement {
  static get styles() {
    return css`
      :host {
        display: flex;
        position: relative;
      }
      button {
        outline: none;
        font-family: inherit;
        font-size: 1.2rem;
        margin: 0.5rem;
        height: 4rem;
        min-width: 9rem;
        border: 1px solid #ccc;
        border-radius: 8px;
        color: #999;
        background-color: #0f1726;
        flex: 1;
      }
      button:active {
        border-color: rgba(19, 211, 69, .9);
        box-shadow: 0 0 4px rgba(19, 211, 69);
        color: rgba(19, 211, 69, .9);
        text-shadow: 0 0 4px rgba(19, 211, 69);
      }
      .firing-led {
        position: absolute;
        right: 1rem;
        top: 1rem;
        width: 1rem;
        height: 1rem;
        border-radius: 4px;
        background-color: rgba(99, 99, 99, .9);
      }
      .firing-led[active] {
        background-color: rgba(19, 211, 69, .9);
        box-shadow: inset 0 0 1rem rgba(19, 211, 69, .9), 0 0 1rem rgba(19, 211, 69, .9);
      }
      .fired-marker-container {
        position: absolute;
        top: 1rem;
        left: 1rem;
      }
      .fired-marker {
        background-color: rgba(223, 10, 10, .7);
        width: 1rem;
        height: 0.3rem;
        transform: skew(-25deg);
        margin-bottom: .3rem;
      }

      @media (min-width: 768px) {
        button {
          height: 5rem;
        }
      }
    `;
  }

  static properties = {
    index: { type: Number },
    firing: { type: String },
    firedcount: { type: Number }
  };

  connectedCallback() {
    this.fsm = new MachineController(this, fireButtonMachine, {
      context: {
        ...fireButtonMachine.context,
        requestFire: this.requestFire.bind(this),
      },
      devTools: true
    });
    super.connectedCallback();
  }

  willUpdate(changedProps) {
    const { send } = this.fsm;
    if (changedProps.has('firing')) {
      send(this.firing === 'on' ? 'FIRING_STARTED' : 'FIRING_STOPPED');
    }
  }

  handleClick() {
    const { send } = this.fsm;
    send('FIRE_CLICKED');
  }

  requestFire() {
    this.dispatchEvent(new CustomEvent('requestFire', { bubbles: true, composed: true }));
  }

  render() {
    const { state } = this.fsm;
    const isFiring = state.matches('firing');
    const firedMarkers = [];
    for (let i = 0; i < this.firedcount; i++) {
      firedMarkers.push(html`<div class="fired-marker"></div>`);
    }
    return html`
      <button @click="${this.handleClick}">FIRE ${this.index}</button>
      <div class="firing-led" ?active=${isFiring}></div>
      <div class="fired-marker-container">
        ${firedMarkers}
      </div>
    `;
  }
}

customElements.define('launcher-fire-button', FireButton);
