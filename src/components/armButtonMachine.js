import { assign, createMachine } from 'xstate';

export const armButtonMachine = createMachine({
  predictableActionArguments: true,
  id: 'armButtonMachine',
  initial: 'initial',
  context: {
    countdownValue: 3,
    isSystemArmed: false
  },
  states: {
    initial: {
      always: [
        {
          cond: 'isSystemArmed',
          target: 'armed'
        },
        {
          target: 'disarmed'
        }
      ]
    },
    armed: {
      initial: 'idle',
      states: {
        idle: {
          on: {
            DISARMED_CLICKED: {
              actions: 'disarmSystem',
              target: 'disarming'
            }
          }
        },
        disarming: {
          after: {
            2000: {
              target: 'idle'
            }
          }
        }
      },
      on: {
        SYSTEM_DISARMED: {
          target: 'disarmed'
        }
      }
    },
    disarmed: {
      initial: 'idle',
      states: {
        idle: {
          on: {
            ARM_PRESSED: {
              target: 'pressing'
            }
          }
        },
        arming: {
          after: {
            2000: {
              target: 'idle'
            }
          }
        },
        pressing: {
          entry: 'resetCountdown',
          invoke: {
            src: 'armDelay'
          },
          on: {
            ARM_RELEASED: {
              target: 'idle'
            },
            PRESS_DELAY_TICK: [
              {
                target: 'arming',
                cond: 'isCountdownComplete',
                actions: 'armSystem'
              },
              {
                actions: 'decrementCountdown'
              }
            ]
          }
        }
      },
      on: {
        SYSTEM_ARMED: {
          target: 'armed'
        }
      }
    }
  }
}, {
  actions: {
    armSystem: ({ requestSystemArm }) => requestSystemArm(),
    disarmSystem: ({ requestSystemDisarm }) => requestSystemDisarm(),
    decrementCountdown: assign({
      countdownValue: ({ countdownValue }) => countdownValue >= 1 ? countdownValue - 1 : 0
    }),
    resetCountdown: assign({ countdownValue: 3 })
  },
  guards: {
    isCountdownComplete: ({ countdownValue }) => countdownValue <= 1,
    isSystemArmed: ({ isSystemArmed }) => isSystemArmed
  },
  services: {
    armDelay: () => send => {
      const tick = setInterval(() => {
        send('PRESS_DELAY_TICK');
      }, 1000);
      return () => clearInterval(tick);
    }
  }
});
