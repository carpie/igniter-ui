import { css, html, LitElement } from 'lit';

import { MachineController } from '../controllers/MachineController.js';

import { armButtonMachine } from './armButtonMachine.js';

class ArmButton extends LitElement {
  static get styles() {
    return css`
      :host {
        display: flex;
      }
      button {
        outline: none;
        font-family: inherit;
        font-size: 1.2rem;
        margin: 0.5rem;
        height: 4rem;
        border: 1px solid #ccc;
        border-radius: 8px;
        color: #999;
        background-color: #0f1726;
        flex: 1;
      }
      button:active {
        border-color: rgba(19, 211, 69, .9);
        box-shadow: 0 0 4px rgba(19, 211, 69);
        color: rgba(19, 211, 69, .9);
        text-shadow: 0 0 4px rgba(19, 211, 69);
      }

      @media (min-width: 768px) {
        button {
          height: 5rem;
        }
    `;
  }

  static properties = {
    armed: { type: Boolean }
  };

  connectedCallback() {
    this.fsm = new MachineController(this, armButtonMachine, {
      context: {
        isSystemArmed: this.armed,
        requestSystemArm: this.requestSystemArm.bind(this),
        requestSystemDisarm: this.requestSystemDisarm.bind(this),
      },
      devTools: true
    });
    super.connectedCallback();
  }

  willUpdate(changedProps) {
    const { send } = this.fsm;
    if (changedProps.has('armed')) {
      send(this.armed ? 'SYSTEM_ARMED' : 'SYSTEM_DISARMED');
    }
  }

  handlePress() {
    const { send } = this.fsm;
    send('ARM_PRESSED');
  }

  handleRelease() {
    const { send } = this.fsm;
    send('ARM_RELEASED');
  }

  handleClick() {
    const { send } = this.fsm;
    send('DISARMED_CLICKED');
  }

  requestSystemArm() {
    this.dispatchEvent(new CustomEvent('requestSystemArm', { bubbles: true, composed: true }));
  }

  requestSystemDisarm() {
    this.dispatchEvent(new CustomEvent('requestSystemDisarm', { bubbles: true, composed: true }));
  }

  render() {
    const { context, state } = this.fsm;
    if (state.matches('armed')) {
      const text = state.matches('armed.disarming') ? 'DISARMING...' : 'TAP TO DISARM';
      return html`
        <button @click="${this.handleClick}">${text}</button>
      `;
    } else {
      let text = 'HOLD TO ARM';
      if (state.matches('disarmed.pressing')) {
        text = `HOLD FOR ${context.countdownValue}...`;
      }
      else if (state.matches('disarmed.arming')) {
        text = `ARMING...`;
      }
      return html`
        <button @pointerdown="${this.handlePress}" @pointerup="${this.handleRelease}">${text}</button>
      `;
    }
  }
}

customElements.define('launcher-arm-button', ArmButton);
