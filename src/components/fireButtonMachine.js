import { createMachine } from 'xstate';

export const fireButtonMachine = createMachine({
  predictableActionArguments: true,
  id: 'fireButtonMachine',
  context: {
  },
  initial: 'idle',
  states: {
    idle: {
      on: {
        FIRING_STARTED: {
          target: 'firing'
        },
        FIRE_CLICKED: {
          actions: 'requestFire',
          target: 'requestingFire'
        }
      }
    },
    firing: {
      on: {
        FIRING_STOPPED: {
          target: 'idle'
        }
      }
    },
    requestingFire: {
      after: {
        2000: {
          target: 'idle'
        }
      },
      on: {
        FIRING_STARTED: {
          target: 'firing'
        }
      }
    }
  }
}, {
  actions: {
    requestFire: ({ requestFire }) => requestFire()
  }
});
