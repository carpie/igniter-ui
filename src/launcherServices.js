import.meta.hot; // bug workaround to make SNOWPACK_ENV available

import * as mqtt from 'mqtt/dist/mqtt';

export const manageMqtt = () => (send) => {
  const client = mqtt.connect(`${__SNOWPACK_ENV__.SNOWPACK_PUBLIC_MQTT_URL}`);

  client.on('connect', () => {
    console.log('connected');
    client.subscribe('/ignite/connected');
    client.subscribe('/ignite/armed');
    client.subscribe('/ignite/relay/#');
    send({ type: 'SERVER_CONNECTED', client });
  });

  client.on('offline', () => {
    console.log('client went offline');
    send('LOST_CONNECTION');
  });

  client.on('error', error => {
    console.log('error occurred', error);
    send('LOST_CONNECTION');
  });

  client.on('message', (topic, msg) => {
    const strMsg = msg.toString();
    // console.log('topic', topic, 'msg', strMsg);
    if (topic === '/ignite/connected') {
      if (strMsg === 'true') {
        send('CONTROLLER_ACTIVE');
      } else {
        send('CONTROLLER_INACTIVE');
      }
    } else if (topic.startsWith('/ignite/relay/')) {
      const relay = parseInt(topic.split('/').pop(), 10);
      send({ type: 'RELAY_STATE_UPDATE', index: relay, state: strMsg });
    } else if (topic.startsWith('/ignite/armed')) {
      if (strMsg === 'true') {
        send('SYSTEM_ARMED');
      } else {
        send('SYSTEM_DISARMED');
      }
    }
  });
};
