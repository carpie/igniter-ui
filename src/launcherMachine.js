import { assign, createMachine } from 'xstate';

import { manageMqtt } from './launcherServices.js';

export const launcherMachine = createMachine({
  predictableActionArguments: true,
  id: 'launcherMachine',
  context: {
    firedCounts: Array.from(Array(12)).map(() => 0),
    relayStates: Array.from(Array(12)).map(() => 'off')
  },
  initial: 'mqtt',
  states: {
    mqtt: {
      invoke: {
        src: 'manageMqtt'
      },
      type: 'parallel',
      states: {
        network: {
          initial: 'connecting',
          states: {
            connecting: {
              on: {
                SERVER_CONNECTED: {
                  target: 'connected',
                  actions: 'storeMqttClient'
                }
              }
            },
            connected: {}
          }
        },
        controller: {
          initial: 'inactive',
          states: {
            active: {},
            inactive: {},
          },
          on: {
            CONTROLLER_ACTIVE: 'controller.active',
            CONTROLLER_INACTIVE: 'controller.inactive'
          }
        },
        fireControl: {
          initial: 'disarmed',
          states: {
            disarmed: {
              on: {
                REQUEST_SYSTEM_ARM: {
                  actions: 'requestSystemArm'
                },
                SYSTEM_ARMED: {
                  target: 'armed'
                }
              }
            },
            armed: {
              on: {
                REQUEST_FIRE_RELAY: {
                  actions: 'fireRelay'
                },
                REQUEST_SYSTEM_DISARM: {
                  actions: 'requestSystemDisarm'
                },
                SYSTEM_DISARMED: {
                  target: 'disarmed'
                },
              }
            }
          },
          on: {
            CLEAR_FIRED_COUNTS: {
              actions: 'clearFiredCounts'
            }
          }
        }
      },
      on: {
        LOST_CONNECTION: {
          target: ['mqtt.network.connecting', 'mqtt.controller.inactive', 'mqtt.fireControl.disarmed']
        },
        RELAY_STATE_UPDATE: {
          actions: 'storeRelayState'
        }
      }
    }
  }
}, {
  actions: {
    clearFiredCounts: assign({
      firedCounts: Array.from(Array(12)).map(() => 0)
    }),
    fireRelay: ({ mqttClient }, { index }) => mqttClient.publish('/ignite/fire', `${index}`),
    requestSystemArm: ({ mqttClient }) => mqttClient.publish('/ignite/arm', '1'),
    requestSystemDisarm: ({ mqttClient }) => mqttClient.publish('/ignite/arm', '0'),
    storeMqttClient: assign({
      mqttClient: (_, { client }) => client
    }),
    storeRelayState: assign({
      firedCounts: ({ firedCounts }, { index, state }) => {
        const counts = [...firedCounts];
        if (state === 'on') {
          counts[index]++
        }
        return counts;
      },
      relayStates: ({ relayStates }, { index, state }) => {
        const states = [...relayStates];
        if (index >= 0 && index < states.length) {
          states[index] = state;
        }
        return states;
      }
    })
  },
  services: {
    manageMqtt
  }
});
