describe('happy path', () => {
  beforeEach(() => {
    cy.task('controller', { action: 'reset' });
    cy.task('monitor', { action: 'start' });
  });

  afterEach(() => {
    cy.task('monitor', { action: 'stop' });
  });

  it('works as expected in the ideal case', () => {
    cy.visit('/');
    cy.get('[data-testid="networkStatus"]').should('include.text', 'CONNECTED');
    cy.get('[data-testid="controllerStatus"]').invoke('text').should('match', /[\s]+INACTIVE/);
    // Simulate the controller connecting
    cy.task('controller', { action: 'activate' });
    cy.get('[data-testid="controllerStatus"]').invoke('text').should('match', /[\s]+ACTIVE/);
    cy.get('[data-testid="armedStatus"]').should('include.text', 'DISARMED');

    // Pressing and releasing ARM before 5 seconds should not arm system
    cy.contains('HOLD TO ARM').trigger('pointerdown');
    cy.contains('HOLD FOR 2').should('exist');
    cy.contains('HOLD FOR 2').trigger('pointerup');
    cy.contains('HOLD TO ARM').should('exist');

    // Pressing and holding ARM for 5 seconds should arm system
    cy.contains('HOLD TO ARM').trigger('pointerdown');
    cy.contains('ARMING').should('exist');
    cy.contains('ARMING').trigger('pointerup');
    // Should have requested the system to arm
    cy.task('monitor', { action: 'getLogs' }).then(logs => {
      expect(logs).to.contain('/ignite/arm 1');
    });
    cy.task('controller', { action: 'reportArmed' });
    cy.contains('TAP TO DISARM').should('exist');
    cy.get('[data-testid="armedStatus"]').invoke('text').should('match', /[\s]+ARMED/);

    // Pressing the fire button now fires the corresponding relay
    cy.task('monitor', { action: 'reset' });
    cy.get('launcher-fire-button[index="0"]').within(() => {
      cy.contains('FIRE 0').click();
      // Should send a fire command for relay 0
      cy.task('monitor', { action: 'getLogs' }).then(logs => {
        expect(logs).to.contain('/ignite/fire 0');
      });
      cy.task('controller', {
        action: 'reportRelayState',
        relay: 0,
        relayState: 'on'
      });
      cy.get('div.firing-led').should('have.attr', 'active');
      cy.get('div.fired-marker').should('have.lengthOf', 1);
      // wait for firing to clear
      cy.task('controller', {
        action: 'reportRelayState',
        relay: 0,
        relayState: 'off'
      });
      cy.get('div.firing-led').should('not.have.attr', 'active');

      // clicking again will add a firing-led
      cy.contains('FIRE 0').click();
      cy.task('controller', {
        action: 'reportRelayState',
        relay: 0,
        relayState: 'on'
      });
      cy.get('div.firing-led').should('have.attr', 'active');
      cy.task('controller', {
        action: 'reportRelayState',
        relay: 0,
        relayState: 'off'
      });
      cy.get('div.fired-marker').should('have.lengthOf', 2);
    });

    // Pressing a different fire button now fires the corresponding relay
    cy.task('monitor', { action: 'reset' });
    cy.get('launcher-fire-button[index="7"]').within(() => {
      cy.contains('FIRE 7').click();
      // Should send a fire command for relay 0
      cy.task('monitor', { action: 'getLogs' }).then(logs => {
        expect(logs).to.contain('/ignite/fire 7');
      });
      cy.task('controller', {
        action: 'reportRelayState',
        relay: 7,
        relayState: 'on'
      });
      cy.get('div.firing-led').should('have.attr', 'active');
      cy.get('div.fired-marker').should('have.lengthOf', 1);
      // wait for firing to clear
      cy.task('controller', {
        action: 'reportRelayState',
        relay: 7,
        relayState: 'off'
      });
      cy.get('div.firing-led').should('not.have.attr', 'active');
    });

    // Pressing the clear fire counts button clears the fire counts
    cy.contains('CLEAR FIRED COUNTS').click();
    cy.get('launcher-fire-button[index="0"]').find('div.fired-marker').should('have.lengthOf', 0);
    cy.get('launcher-fire-button[index="7"]').find('div.fired-marker').should('have.lengthOf', 0);

    // Clicking disarm disarms the system
    cy.task('monitor', { action: 'reset' });
    cy.contains('TAP TO DISARM').click();
    // Should send a disarm command
    cy.task('monitor', { action: 'getLogs' }).then(logs => {
      expect(logs).to.contain('/ignite/arm 0');
    });
    cy.task('controller', { action: 'reportDisarmed' });
    cy.get('[data-testid="armedStatus"]').should('include.text', 'DISARMED');

    // System reports when controller goes offline
    cy.task('controller', { action: 'deactivate' });
    cy.get('[data-testid="controllerStatus"]').invoke('text').should('match', /[\s]+INACTIVE/);
  });
});
